var map_filters = [];
var filters_selected = [];
var all_markers = [];

/**
 * Genera il contenuto del fumetto a comparsa al clic sul marcatore in mappa
 */
function POIinfo(element, map_filters) {
    let tags = element.tags;
    let id = element.id;
    let row;
    let startingHtml = '';
    const link = document.createElement('a');
    const table = document.createElement('table');
    const div = document.createElement('div');

    link.href = `https://www.openstreetmap.org/edit?editor=id&node=${id}`;
    link.target = '_blank';
    link.appendChild(document.createTextNode('✏️ Modifica su OpenStreetMap'));

    table.style.borderSpacing = '10px';
    table.style.borderCollapse = 'collapse';

    let iconOpened = '❔';
    var allHours = '';

    // Costruzione fumetto personalizzato
    if (tags['opening_hours'] !== undefined) {
        date = tags['opening_hours'];

        // Inizializzazione opening_hours
        let locale = navigator.language;
        let oh = new opening_hours(date, {}, { 'locale': locale });

        // Verifica se l'attività è aperta o chiusa
        var is_open = oh.getState();
        iconOpened = is_open ? '🟢' : '🔴';

        // Prettify the value
        allHours = oh.prettifyValue({
            conf: {
                locale: 'it',
                rule_sep_string: '<br>',
                print_semicolon: false,
                sep_one_day_between: ', '
            }
        });
        
        // Console outputs.
        var it = oh.getIterator( new Date() );
        var prettyHours = OpeningHoursTable.drawTableAndComments(oh, it);
    }

    // Show filter on popup if filter exists on POI
    icons = [];
    Object.keys(tags).forEach((tag_name) => {
        Object.keys(map_filters).forEach((filter) => {
            marker_filter = tag_name+"="+tags[tag_name];
            map_filter = map_filters[filter].replace(/"/g, '');

            if (marker_filter == map_filter) {
                icons.push( filter.substring(0, 2) );
            }
        });
    });

    // Title
    startingHtml = '<h2>' + iconOpened + ' ' + (tags['name'] !== undefined ? tags['name']: '<em>senza nome</em>') + '</h2>';

    startingHtml += (icons.length > 0 ? '<h3>' + icons.join(' ') + '</h3>' : '');



    if (allHours) {
        startingHtml += prettyHours + '<br>' + allHours + '<br><br>';
    }
    
    for (const key in tags) {
        row = table.insertRow(0);
        value = tags[key];

        row.insertCell(0).appendChild(document.createTextNode(key));
        row.insertCell(1).appendChild(document.createTextNode(value));
    }

    div.innerHTML = startingHtml;

    // Call telephone
    if (tags['contact:phone'] !== undefined) {
        phone = tags['contact:phone'].split(';');

        for(p=0; p<phone.length; p++) {
            div.innerHTML += '\
                <a class="btn btn-phone btn-block" href="tel:' + phone[p] + '"><i class="fa fa-phone"></i> ' + phone[p] + '</a>';
        }
    } else if (tags['phone'] !== undefined) {
        phone = tags['phone'].split(';');

        for(p=0; p<phone.length; p++) {
            div.innerHTML += '\
                <a class="btn btn-phone btn-block" href="tel:' + phone[p] + '"><i class="fa fa-phone"></i> ' + phone[p] + '</a>';
        }
    }

    // Call mobile
    if (tags['contact:mobile'] !== undefined) {
        phone = tags['contact:mobile'].split(';');

        for(p=0; p<phone.length; p++) {
            div.innerHTML += '\
                <a class="btn btn-mobile btn-block" href="tel:' + phone[p] + '"><i class="fa fa-mobile"></i> ' + phone[p] + '</a>';
        }
    } else if (tags['mobile'] !== undefined) {
        phone = tags['mobile'].split(';');

        for(p=0; p<phone.length; p++) {
            div.innerHTML += '\
                <a class="btn btn-mobile btn-block" href="tel:' + phone[p] + '"><i class="fa fa-mobile"></i> ' + phone[p] + '</a>';
        }
    }

    // Send email
    if (tags['contact:email'] !== undefined) {
        div.innerHTML += '\
            <a class="btn btn-email btn-block" href="mailto:' + tags['contact:email'] + '"><i class="fa fa-envelope"></i> EMAIL</a>';
    }

    // Visit website
    if (tags['contact:website'] !== undefined) {
        div.innerHTML += '\
            <a class="btn btn-website btn-block" href="' + tags['contact:website'] + '" target="_blank"><i class="fa fa-globe"></i> SITO WEB</a>';
    }

    // Go to map
    div.innerHTML += '\
        <a class="btn btn-geo btn-block" href="geo:' + element['lat'] + ',' + element['lon'] + '"><i class="fa fa-map-marker"></i> INDICAZIONI</a>';

    // Show all tags
    div.innerHTML += '\
    <div>\
        <hr>\
        <a class="clickable" onclick="icon = this.querySelectorAll(\'i\')[0]; icon.classList.toggle(\'fa-plus\'); icon.classList.toggle(\'fa-minus\'); next(this).style.display = next(this).style.display == \'none\' ? \'block\' : \'none\';"><i class="fa fa-plus"></i> Mostra tutte le etichette</a>\
        <div class="all-osm-tags" style="display:none;">' + table.outerHTML + '</div>\
    </div>';

    div.appendChild(document.createElement('br'));
    div.appendChild(link);

    return {
        'html': div,
        'is_open': is_open
    };
}

function build_map_filters(filters){
    document.querySelector("#map-filters").innerHTML = '<h3 onclick="document.querySelector(\'#map-filters\').classList.toggle(\'closed\'); document.querySelector(\'#map-filters i\').classList.toggle(\'fa-plus\'); document.querySelector(\'#map-filters i\').classList.toggle(\'fa-minus\'); document.querySelectorAll(\'.leaflet-bottom\')[0].classList.toggle(\'hidden\'); document.querySelectorAll(\'.leaflet-bottom\')[1].classList.toggle(\'hidden\');"><i class="fa fa-plus"></i> Filtri (<span id="filter-counter">0</span>)</h3><div class="checkboxes"></div>';

    count = 0;
    for(let filter in filters){
        document.querySelector("#map-filters div").innerHTML +=
            "<div class='col'>\
                <div class='check'>\
                    <input type='checkbox' id='checkbox-" + count + "' value='" + ((filters[filter])) + "' onclick=\"filter_markers(this.value);\">\
                    <label for='checkbox-" + count + "'>\
                        <div class='box'><i class='fa fa-check'></i></div>" + filter + "\
                    </label>\
                </div>\
            </div>";
        count++;
    }
}

function filter_markers(filter, markers=[]){
    // If no marker is selected, then filter all markers
    if (markers.length == 0) {
        markers = all_markers;
    }

    // If no filter is selected, then use the selected filters
    if (filter !== null){
        if (filters_selected.includes(filter)) {
            filters_selected = filters_selected.filter(function(e) { return e !== filter })
        } else {
            filters_selected.push(filter);
        }
    }



    // Update filter counter
    document.querySelector("#filter-counter").innerHTML = filters_selected.length;
    
    // Reset opacity for all markers if no filter is selected
    if (filters_selected.length == 0) {
        for (let marker of markers) {
            marker.setOpacity(1);
            setPopupContent(marker);
        }
    }
    else {
        for(let marker of markers){
            var total_filters = 0;
            var filters_match_counter = 0;

            // The most complex filter can be like this:
            // amenity=pharmacy|healthcare=pharmacy;dispensing=no
            for(let filter of filters_selected){
                multifilters = filter.split(';');
                total_filters = multifilters.length;

                if (marker.tags['name'] == 'Farmacia Montagna' && marker.tags['dispensing'] == 'no') {
                    a = 1;
                }

                // Loop the AND filters
                for(let multifilter of multifilters){
                    or_parts = multifilter.split('|');

                    // Loop the OR filters
                    var or_matches = false;
                    for (let or_part of or_parts) {
                        parts = or_part.split('=');
                        label_name = parts[0].replace(/"/g, '');
                        label_value = parts[1].replace(/"/g, '');

                        // Can be more than one tag separated by comma
                        if (marker.tags[label_name] !== undefined) {
                            if(marker.tags[label_name] == label_value){
                                or_matches = true;
                            }
                        }
                    }

                    // If at least one OR filter matches, filter is satisfied
                    if (or_matches) {
                        filters_match_counter++;
                    }
                }
            }

            if (filters_match_counter == total_filters) {
                marker.setOpacity(1);
                setPopupContent(marker);
            } else {
                marker.setOpacity(0);
                marker.off('click');
                marker.unbindPopup();
            }
        }
    }
}

function setPopupContent(marker){
    // Hide leaflet map container
    const popup = L.popup().setContent(marker.html);
    marker.bindPopup(popup, { maxWidth: "auto" });
    
    // When clicked on marker, if popup is open then hide leaflet controls
    marker.on('click', function(){
        if(marker.isPopupOpen()){
            document.querySelectorAll('.leaflet-bottom')[0].classList.add('hidden');
        }
    });

    // When popup is closed, show leaflet controls
    marker.getPopup().on('remove', function() {
        if (document.querySelector('#map-filters').classList.contains('closed')) {
            document.querySelectorAll('.leaflet-bottom')[0].classList.remove('hidden');
        }
    });
}

String.prototype.addSlashes = function() { 
    return this.replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

function next(el, selector) {
    const nextEl = el.nextElementSibling;
    if (!selector || (nextEl && nextEl.matches(selector))) {
        return nextEl;
    }
    return null;
}