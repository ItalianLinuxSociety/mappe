---
title: Dove mangiare
description: Trova locali per mangiare
slug: "dove-mangiare"
layout: mappe
overpassQuery: '
  (
    node[amenity=bar];
    node[amenity=pub];
    node[amenity=cafe];
    node[amenity=restaurant];
    node[cuisine=pizza];
    node[amenity=ice_cream];
    node[cuisine=ice_cream];
    node[amenity=fast_food];
  );'
mapFilters:
    - name: '🍷 Bar'
      query: 'amenity=bar'
    - name: '🍸 Pub'
      query: 'amenity=pub'
    - name: '☕ Caffetteria'
      query: 'amenity=cafe'
    - name: '🍴 Ristorante'
      query: 'amenity=restaurant'
    - name: '🍕 Pizzeria'
      query: 'cuisine=pizza'
    - name: '🍦 Gelateria'
      query: 'amenity=ice_cream|cuisine=ice_cream'
    - name: '🍔 Mangia e fuggi'
      query: 'amenity=fast_food'
    - name: '🥬 Vegetariano'
      query: '"diet:vegetarian"="yes"'
    - name: '🥬 Esclusivamente vegetariano'
      query: '"diet:vegetarian"="only"'
    - name: '🌿 Vegano'
      query: '"diet:vegan"="yes"'
    - name: '🌿 Esclusivamente vegano'
      query: '"diet:vegan"="only"'
    - name: '🌾 Senza glutine'
      query: '"diet:gluten_free"="yes"'
    - name: '🌾 Esclusivamente senza glutine'
      query: '"diet:gluten_free"="only"'
    - name: '♿ Accesso sedia rotelle'
      query: '"wheelchair"="yes"'
---
