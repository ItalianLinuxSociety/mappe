---
title: Salute
description: Trova luoghi per la salute
slug: "salute"
layout: mappe
overpassQuery: '(node[amenity=hospital];node[healthcare=doctors];node[healthcare=physiotherapist];node[healthcare=dentist];node[healthcare=clinic];node[amenity=pharmacy];node[healthcare=pharmacy];node[amenity=veterinary];);'
mapFilters:
    - name: '🏥 Ospedale'
      query: 'amenity=hospital'
    - name: '🆘 Pronto soccorso'
      query: 'emergency=yes'
    - name: '🧑‍⚕️ Ambulatorio'
      query: 'healthcare=doctor'
    - name: '⚕️ Poliambulatorio'
      query: 'healthcare=clinic'
    - name: '🩼 Fisioterapista'
      query: 'healthcare=physiotherapist'
    - name: '🦷 Dentista'
      query: 'healthcare=dentist'
    - name: '💊 Farmacia'
      query: 'amenity=pharmacy'
    - name: '💊 Parafarmacia'
      query: 'amenity=pharmacy|healthcare=pharmacy;dispensing=no'
    - name: '🐾 Veterinario'
      query: 'amenity=veterinary'
---
