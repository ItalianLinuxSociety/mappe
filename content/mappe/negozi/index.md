---
title: Negozi
description: Cerca negozi originali e di tutte le tipologie
slug: "negozi"
layout: mappe
overpassQuery: '(
      node[shop]
        [shop!=parking]
        [shop!=parking]
        [shop!=laundry];
      node[office];
    )'
mapFilters:
    - name: '🚯 Prodotti sfusi'
      query: 'zero_waste=yes'
    - name: '🚯 Solo prodotti sfusi'
      query: 'zero_waste=only'
    - name: '🛒 Supermercato'
      query: 'shop=supermarket'
    - name: '🌷 Fioreria'
      query: 'shop=florist'
    - name: '🍞 Panificio'
      query: 'shop=bakery'
    - name: '📕 Libreria'
      query: 'shop=books'
    - name: '💅 Estetica'
      query: 'shop=beauty'
    - name: '🚲 Bici'
      query: 'shop=beauty'
---