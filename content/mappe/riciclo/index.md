---
title: Riciclo
description: Trova dove poter riciclare i rifiuti
slug: "riciclo"
layout: mappe
overpassQuery: '(node[amenity=recycling];);'
mapFilters:
    - name: '🟡 Olio da cucina'
      query: '"recycling:cooking_oil"="yes"'
    - name: '🪫 Batterie esauste'
      query: '"recycling:batteries"="yes"'
    - name: '💩 Deiezioni di animali'
      query: '"recycling:animal_waste"="yes"'
    - name: '💊 Farmaci'
      query: '"recycling:drugs"="yes"'
    - name: '🧻 Carta'
      query: '"recycling:paper"="yes"'
    - name: '♳ Plastica'
      query: '"recycling:plastic"="yes"'
    - name: '🔧 Metallo'
      query: '"recycling:metal"="yes"'
    - name: '🫙 Vetro'
      query: '"recycling:glass"="yes"'
    - name: '📱 Elettronica'
      query: '"recycling:electronics"="yes"'
    - name: '🍏 Verde'
      query: '"recycling:green_waste"="yes"'
    - name: '♻️ Ecocentro'
      query: '"recycling_type"="centre"'
    - name: '🗑️ Cestino'
      query: '"recycling:waste"="yes"'
      
      

---
