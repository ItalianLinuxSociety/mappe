---
title: 🧩 Come funziona
description: Il portale delle mappe libere più flessibile di tutti
IgnoreTableOfContents: true
images:
 - puzzle-pieces-1925422_1280.jpg
---

Il progetto **Mappe** è ideato dalla [sezione locale di ILS di Este](https://este.linux.it) da **Fabio Lovato**.

E' un sito web per la consultazione di mappe a tema. Il codice del sito web è libero e scaricabile da [gitlab.com/ItalianLinuxSociety/mappe](https://gitlab.com/ItalianLinuxSociety/mappe). Le [4 libertà fondamentali](https://www.linux.it/softwarelibero/) del software libero permettono di replicare questo sito web per proporre mappe di temi diversi. Le mappe utilizzate sono anch'esse libere e sono fornite dal progetto OpenStreetMap.

Su questo sito web, le mappe proposte sono per gli usi più comuni e l'aspetto interessante è che sono **filtrabili** per più criteri che possono essere scelti in combinazione tra loro (esempio: una pizzeria che usa ingredienti senza glutine, o ristoranti vegani e accessibili con sedia a rotelle). Ecco che lo sforzo dei contributori di **[OpenStreetMap](https://openstreetmap.org)**, la fonte con licenza libera dalla quale vengono popolate queste mappe, viene concentrato nel fornire delle mappe molto utili e filtrabili. Inoltre, lo spirito di condivisione e apertura, permette di contribuire a creare nuovi filtri e mappe ancora più interessanti!

*Foto di [congerdesign](https://pixabay.com/it/users/congerdesign-509903/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1925422) da [Pixabay](https://pixabay.com/it//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1925422)*


*Il logo è fornito da [Freepik](https://www.freepik.com/free-vector/abstract-colorful-bullet-point-collection_4060870.htm#query=marker&position=16&from_view=search&track=sph)*

*L'icona del marcatore su mappa è fornita da [Freepik](https://www.freepik.com/icon/placeholder_8830930#fromView=search&term=marker&page=3&position=5&track=ais)*

*Le emoji utilizzate sono caratteri in UTF-8 trovati con l'aiuto di [Emojipedia](https://emojipedia.org/)*