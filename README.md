# Mappe

Intento del progetto è creare un indice di mappe facilmente accessibili e consultabili su temi specifici e verticali, estrapolando i dati da OpenStreetMap e altre fonti libere.

## Come compilare
```bash
git clone https://gitlab.com/ItalianLinuxSociety/mappe.git
cd mappe
git submodule add https://github.com/luizdepra/hugo-coder.git themes/hugo-coder
```

Per testare in tempo reale:
```bash
hugo server -D
```

## Organizzazione contenuti

Tutti i contenuti sono nella cartella `content` e hanno lo stesso nome dei menu.
