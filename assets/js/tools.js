function ready(fn) {
  if (document.readyState !== 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}


ready(function () {
  document.querySelectorAll('.has-children').forEach((el, i) => {
    el.addEventListener('click', () => {
      document.querySelector('.childrens').classList.toggle('hidden');
    });
  });
});

function next(el, selector) {
  const nextEl = el.nextElementSibling;
  if (!selector || (nextEl && nextEl.matches(selector))) {
    return nextEl;
  }
  return null;
}
